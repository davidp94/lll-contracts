# lll-contracts

These are a few simple LLL examples.

Creation patterns (various approaches to Factory and the like) have been
moved to a [separate repository](https://gitlab.com/veox/lll-creation-patterns).


## License

Blanket-covered by GPLv3 (see [`LICENSE.txt`](LICENSE.txt)).
